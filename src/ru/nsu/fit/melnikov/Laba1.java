package ru.nsu.fit.melnikov;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.Character.isLetterOrDigit;

public class Laba1 {
    private static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue(Map<K, V> map) {
        Map<K, V> result = new LinkedHashMap<>();
        Stream<Map.Entry<K, V>> st = map.entrySet().stream();

        st.sorted(Comparator.comparing(Map.Entry::getValue))
                .forEach(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }

    public static void main(String[] args) {
        try  (Reader reader =
                new InputStreamReader(new FileInputStream(args[0]))){
            Map<String, Integer> map = new HashMap<>();
            StringBuilder word = new StringBuilder();
            int buf, numberOfWords = 0;
            boolean flag = true;
            String s;
            Integer t;
          do {
                buf = reader.read();
                if (isLetterOrDigit(buf)) {
                    flag = true;
                    word.append((char) buf);
                } else if (flag) {
                    flag = false;
                    numberOfWords++;
                    s = word.toString();
                    t = map.getOrDefault(s, 1);
                    map.put(s, t);
                    word = new StringBuilder();
                }
            }   while (buf != -1);
            map = sortByValue(map);
            int finalNumberOfWords = numberOfWords;
            map.forEach((String key, Integer value) -> System.out.println(key + " : " + value + " : " +
                    String.format("%.2g%n", ((float) value / (float) finalNumberOfWords) * 100.0)));
        } catch (IOException e) {
            System.err.println("Error while reading file:" + e.getLocalizedMessage());
        }
    }
}
